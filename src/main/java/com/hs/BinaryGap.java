package com.hs;

// 1.1 https://codility.com/programmers/task/binary_gap/

public class BinaryGap {
    public int solution(int N) {
        String binaryStr = Integer.toBinaryString(N);
        char[] binArray = binaryStr.toCharArray();
        int currGap = 0;
        int maxGap = 0;
        boolean insideGap = false;
        for (int i = 0; i < binArray.length; i++) {
            if (!insideGap && binArray[i] == '1') {
                insideGap = true;
                continue;
            }
            if (insideGap && binArray[i] == '0') {
                currGap++;
                continue;
            }
            if (insideGap && binArray[i] == '1') {
                maxGap = currGap > maxGap ? currGap : maxGap;
                currGap = 0;
            }
        }
        return maxGap;
    }
}
