package com.hs;

//5.3 https://codility.com/programmers/task/genomic_range_query/

class GenomicRangeQuery {
    public int[] solution(String S, int[] P, int[] Q) {
        char[] dna = S.toCharArray();
        int[] result = new int[P.length];
        int[][] prefSum = new int[dna.length + 1][3];
        for (int i = 0; i < dna.length; i++) {
            int factor = getFactor(dna[i]);
            prefSum[i + 1][0] = prefSum[i][0];
            prefSum[i + 1][1] = prefSum[i][1];
            prefSum[i + 1][2] = prefSum[i][2];
            if (factor != 4) {
                prefSum[i + 1][factor - 1]++;
            }
        }
        for (int i = 0; i < P.length; i++) {
            int startIndex = P[i];
            int endIndex = Q[i] + 1;
            if (prefSum[endIndex][0] - prefSum[startIndex][0] > 0) {
                result[i] = 1;
            } else if (prefSum[endIndex][1] - prefSum[startIndex][1] > 0) {
                result[i] = 2;
            } else if (prefSum[endIndex][2] - prefSum[startIndex][2] > 0) {
                result[i] = 3;
            } else {
                result[i] = 4;
            }
        }
        return result;
    }

    private int getFactor(char c) {
        if (c == 'A') return 1;
        if (c == 'C') return 2;
        if (c == 'G') return 3;
        if (c == 'T') return 4;
        return 0;
    }
}
