package com.hs;

// 6.4 https://codility.com/programmers/task/number_of_disc_intersections/

class DiscIntersections {
    public int solution(int[] A) {
        long[][] range = new long[A.length][2];
        for (int i = 0; i < A.length; i++) {
            range[i][0] = (long) i - A[i];
            range[i][1] = (long) i + A[i];
        }
        int count = 0;
        for (int i = 0; i < A.length; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if ((range[i][0] >= range[j][0] && range[i][0] <= range[j][1])
                        || (range[i][1] >= range[j][0] && range[i][1] <= range[j][1])
                        || (range[j][0] >= range[i][0] && range[j][0] <= range[i][1])
                        || (range[j][1] >= range[i][0] && range[j][1] <= range[i][1])
                        ) {
                    count++;
                    if (count > 10000000) {
                        return -1;
                    }
                }
            }
        }
        return count;
    }
}
