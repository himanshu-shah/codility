package com.hs;

import static org.junit.Assert.assertEquals;

/**
 * Created by Himanshu Shah on 04/07/2016.
 */
public class DiscIntersectionsTest {
    @org.junit.Test
    public void testSolution() throws Exception {
        DiscIntersections ds = new DiscIntersections();
        int intersectingPairs = ds.solution(new int[]{1, 5, 2, 1, 4, 0});
        assertEquals(11, intersectingPairs);
    }

}