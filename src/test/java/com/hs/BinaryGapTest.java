package com.hs;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by Himanshu on 13/07/2016.
 */
public class BinaryGapTest {
    @Test
    public void solution() throws Exception {
        BinaryGap bg = new BinaryGap();
        int result = bg.solution(1041);
        assertEquals(5, result);
        result = bg.solution(15);
        assertEquals(0, result);
    }
}