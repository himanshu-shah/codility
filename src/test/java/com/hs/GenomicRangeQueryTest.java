package com.hs;

import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

/**
 * Created by Himanshu Shah on 04/07/2016.
 */
public class GenomicRangeQueryTest {
    @Test
    public void testSolution() throws Exception {
        GenomicRangeQuery grq = new GenomicRangeQuery();
        int[] queryAnswers = grq.solution("CAGCCTA", new int[]{2, 5, 0}, new int[]{4, 5, 6});
        assertArrayEquals(new int[]{2, 4, 1}, queryAnswers);
    }
}